#!/usr/bin/python
import rospy
from blink1.srv import Blink
from diagnostic_msgs.msg import DiagnosticArray, DiagnosticStatus
import yaml
import time

'''
Searches a DiagnosticArray's status list for a DiagnosticStatus with a matching name
and outputs the level of this DiagnosticStatus as a string
msg - DiagnosticArray message
name - The name field that will be searched for in the DiagnosticStatus list
return The level of the DiagnosticStatus message as a string, or 'NOTFOUND' if the name wasn't found
'''
def get_level(msg, name):
    for status in msg.status:
        if status.name == name:
            if status.level == DiagnosticStatus.OK:
                return 'OK'
            elif status.level == DiagnosticStatus.WARN:
                return 'WARN'
            elif status.level == DiagnosticStatus.ERROR:
                return 'ERROR'
            break
    return 'NOTFOUND'

'''
Checks if a message satisifes a condition as defined in the config yaml file
condition - The name of a condition: and, or, check
sub_conditions - A dictionary containing name, and level if the condition is check.
                 A list of sub conditions if the condition is and/or.
msg - The DiagnosticArray message to check
return True or False based on whether the condition is satisfied.
'''
def satisfies(condition, sub_conditions, msg):
    if condition == 'check':
        for status in msg.status:
            if sub_conditions['level'].upper() in get_level(msg, sub_conditions['name']):
                return True
    elif condition == 'or':
        for sub in sub_conditions:
            if satisfies(sub.keys()[0], sub[sub.keys()[0]], msg):
                return True
    elif condition == 'and':
        for sub in sub_conditions:
            if not satisfies(sub.keys()[0], sub[sub.keys()[0]], msg):
                return False
        return True
    return False

'''
Sets the blink LED based on a dictionary which can contain the following keys
 color: A string, either 'red', 'green', 'blue', 'yellow', 'purple', 'white', or 'off'
 pattern: A string either, 'fade', 'on', or 'blink'
 freq: An int which tells the time to take for fading, or frequency of blinking. Units: ms
action - The dictionary
'''
def set_led(action):
    # Set defaults if certain keys are not present
    if 'color' not in action.keys():
        rospy.logerr('No color found in action, defaulting to "off". Should be a 3 integer tuple, ie. (100, 0, 255) ' +
                     'or one of the following strings: ' + ' '.join(colors.keys()))
    if 'pattern' not in action.keys():
        action['pattern'] = 'on'
    if 'freq' not in action.keys():
        action['freq'] = 1000

    # Make sure the color is valid
    color = colors['off']
    if action['color'].lower() not in colors.keys():
        try:
            temp_color = eval(action['color'])
            if len(temp_color) != 3:
                raise Exception
            color = temp_color
        except:
            rospy.logerr('Failed to parse color "' + str(action['color']) + '". Should be a 3 integer tuple, ie (100, 0, 255) ' +
                         'or one of the following strings: ' + ' '.join(colors.keys()))
    else:
        color = colors[action['color'].lower()]

    # Make sure the pattern is valid
    if action['pattern'].lower() not in patterns.keys():
        rospy.logerr('Pattern "' + action['pattern'] + '" is not a valid option. Valid patterns are: ' + ' '.join(patterns) +
                     '. Defaulting to "on"')
        action['pattern'] = 'on'
        
    # Make sure the frequency is valid
    try:
        action['freq'] = int(action['freq'])
    except:
        rospy.logerr('Freq "' + str(action['freq']) + '" cannot be converted to an integer. Should be an integer. Defaulting to 1000.')
        action['freq'] = 1000
        
        
    blink_srv(patterns[action['pattern'].lower()], action['freq'], *color)

'''
/diagnostics_agg callback
msg - DiagnosticArray message
'''
def diagnostics_agg_callback(msg):
    all_ok = True

    for event in config['events']:
        event = event['event']
        condition = event['condition']
        action = event['action']

        if satisfies(condition.keys()[0], condition[condition.keys()[0]], msg):
            set_led(action)
            all_ok = False
    
    if all_ok:
        set_led(config['default']['action'])
            

if __name__ == '__main__':
    rospy.init_node('blink_diagnostics', anonymous=True)

    config_filename = rospy.get_param('~config', '../launch/blink_config.yaml')
    config = yaml.load(open(config_filename, 'r').read())
    
    rospy.wait_for_service('/blink1/blink')
    blink_srv = rospy.ServiceProxy('/blink1/blink', Blink)

    diagnostics_sub = rospy.Subscriber('/diagnostics_agg', DiagnosticArray, diagnostics_agg_callback)

    colors = {'red': (255, 0, 0),
              'green': (0, 255, 0),
              'blue': (0, 0, 255),
              'yellow': (255, 255, 0),
              'purple': (255, 0, 255),
              'white': (255, 255, 255),
              'off': (0, 0, 0)}
    patterns = {'fade': 1, 'on': 2, 'blink': 3}
    
    blink_srv(2, 0, *colors['off'])

    
    
    rospy.spin()
